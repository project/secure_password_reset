Secure Password Reset
----------------------

Overview:
--------
Makes password reset form more secured by not disclosing valid usernames

Installation:
------------
1. Copy the secure_password_reset directory to the Drupal
   /modules/ directory.
2. Go to "/admin/modules" and enable the module.
Thats all there is to it, the password reset form should
now give the user the same message in both cases
of valid or invalid username
